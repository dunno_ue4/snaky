// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElement.h"
#include "Interactable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnake::AddSnakeElement(int ElementsNum)
{	
	for (int i = 0; i < ElementsNum; ++i)
	{
		FTransform NewTransform;

		if (ElementsNum > 1) // for the first spawn only
		{
			FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
			NewTransform = FTransform(NewLocation);
		}
		else
		{
			NewTransform = FTransform(LastElemPosition);
		}
		ASnakeElement* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElement>(SnakeElementClass, NewTransform); //to make a check for more than 2 elements growing
		NewSnakeElem->SnakeOwner = this;
		//NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}

	//SnakeElements.Last()->SetActorLocation(LastElemPosition);
}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();

	LastElemPosition = SnakeElements.Last()->GetActorLocation();

	for (int i = SnakeElements.Num()-1; i>0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlap(ASnakeElement* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnake::SpeedUp(float DeltaSpeed)
{
	MovementSpeed -= DeltaSpeed; //subtract delta to make movement period shorter.
								 //slowdown if delta is negative
	SetActorTickInterval(MovementSpeed);
}

